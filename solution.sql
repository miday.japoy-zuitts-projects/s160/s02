CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users (
                       id INT NOT NULL AUTO_INCREMENT,
                       email VARCHAR(100) NOT NULL,
                       password VARCHAR(100) NOT NULL,
                       datetime_created DATETIME NOT NULL,
                       PRIMARY KEY (id)
);

CREATE TABLE posts (
                       id INT NOT NULL AUTO_INCREMENT,
                       title VARCHAR(500) NOT NULL,
                       content VARCHAR(5000) NOT NULL,
                       datetime_created DATETIME NOT NULL,
                       user_id INT NOT NULL,
                       PRIMARY KEY (id),
                       CONSTRAINT fk_posts_user_id
                           FOREIGN KEY (user_id)
                               REFERENCES users (id)
                               ON UPDATE CASCADE
                               ON DELETE RESTRICT
);

CREATE TABLE posts (
                       id INT NOT NULL AUTO_INCREMENT,
                       title VARCHAR(500) NOT NULL,
                       content VARCHAR(5000) NOT NULL,
                       datetime_created DATETIME NOT NULL,
                       user_id INT NOT NULL,
                       PRIMARY KEY (id),
                       CONSTRAINT fk_posts_user_id
                           FOREIGN KEY (user_id)
                               REFERENCES users (id)
                               ON UPDATE CASCADE
                               ON DELETE RESTRICT
);

CREATE TABLE post_comments (
                               id INT NOT NULL AUTO_INCREMENT,
                               content VARCHAR(5000) NOT NULL,
                               datetime_created DATETIME NOT NULL,
                               user_id INT NOT NULL,
                               post_id INT NOT NULL,
                               PRIMARY KEY (id),
                               CONSTRAINT fk_posts_user_id
                                   FOREIGN KEY (user_id)
                                       REFERENCES users (id),
                               CONSTRAINT fk_post_comments_post_id
                                   FOREIGN KEY (post_id)
                                       REFERENCES posts (id)
                                       ON UPDATE CASCADE
                                       ON DELETE RESTRICT
);

CREATE TABLE post_likes (
                            id INT NOT NULL AUTO_INCREMENT,
                            datetime_created DATETIME NOT NULL,
                            user_id INT NOT NULL,
                            post_id INT NOT NULL,
                            PRIMARY KEY (id),
                            CONSTRAINT fk_posts_user_id
                                FOREIGN KEY (user_id)
                                    REFERENCES users (id),
                            CONSTRAINT fk_post_comments_post_id
                                FOREIGN KEY (post_id)
                                    REFERENCES posts (id)
                                    ON UPDATE CASCADE
                                    ON DELETE RESTRICT
);